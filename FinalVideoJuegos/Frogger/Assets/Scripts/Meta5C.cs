﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meta5C : MonoBehaviour {

	GameObject ban;
	// Use this for initialization
	void Start () {
		ban = GameObject.Find ("Ban5");
	}

	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D other)
	{

		if (other.gameObject.name == "Rana") {
			if (EstadoRana.met5) {
				EstadoRana.metas += 1;
				EstadoRana.met5 = false;
				Choque.choqu = false;
				MovimientoRana.tp ();
				ban.gameObject.transform.position = new Vector3 (0.778f,1.428f,0);

			}

		}
	}
}
