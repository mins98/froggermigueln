﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meta3C : MonoBehaviour {

	GameObject ban;
	// Use this for initialization
	void Start () {
		ban = GameObject.Find ("Ban3");
	}

	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D other)
	{

		if (other.gameObject.name == "Rana") {
			if (EstadoRana.met3) {
				EstadoRana.metas += 1;
				EstadoRana.met3 = false;
				Choque.choqu = false;
				MovimientoRana.tp ();
				ban.gameObject.transform.position = new Vector3 (-0.017f,1.428f,0);

			}

		}
	}
}
