﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sobre : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.name == "Rana") {
			MuerteAgua.sobre = true;
			EstadoRana.muer = 0;
			EstadoRana.esmov = true;
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.name == "Rana") {
			MuerteAgua.sobre = false;
			EstadoRana.esmov = true;
			MovimientoRana.mover (0);
		}
	}
}
