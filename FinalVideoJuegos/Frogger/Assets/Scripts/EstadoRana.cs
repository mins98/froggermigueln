﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EstadoRana : MonoBehaviour {

	public static int muer=0; //para controlar que tanto toca el agua
	public static bool esmov=false;//para saber si la rana se mueve por un objeto
	public static int vidad=3;//vidas
	public static int metas = 0;//numero de lugares tomados
	public static bool met1=true;
	public static bool met2=true;
	public static bool met3=true;
	public static bool met4=true;
	public static bool met5=true;
	public static int puntua=10;
	public Text puntuatxt;
	public Text vidas;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Destroy (vidaa);
		//float disx = -0.85f;
		//for (int i = 0; i < vidad; i++) {
			//Instantiate (vidaa, new Vector2 (disx, -0.58f), new Quaternion ());
			//disx += 0.20f;
		//}
		if (muer > 1) {
			vidad -= 1;
			muer = 0;
			MovimientoRana.murio ();
		}
		if (vidad == 0) {
			EstadoRana.puntua = 10;
			Application.LoadLevel ("0");
			EstadoRana.metas = 0;
			EstadoRana.met1 = true;
			EstadoRana.met2= true;
			EstadoRana.met3 = true;
			EstadoRana.met4 = true;
			EstadoRana.met5 = true;
		}
		vidas.text="Se tiene "+vidad+" vidas";
		puntuatxt.text = puntua + "";
	}

}
