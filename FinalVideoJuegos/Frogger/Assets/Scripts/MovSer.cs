﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovSer : MonoBehaviour {

	Animator anim;
	private Rigidbody2D cuerpo;
	public float vel;
	bool sentido;
	// Use this for initialization
	void Start () {
		cuerpo = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		if(sentido)
		cuerpo.velocity = new Vector2 (vel, 0);
		else
			cuerpo.velocity = new Vector2 (vel*-1, 0);
	}
	void OnCollisionEnter2D(Collision2D other){
		if (sentido) {
			sentido = false;
			anim.SetBool("cambio",sentido);
			transform.localScale = new Vector3 (1f,1f,1f);
		} else {
			sentido = true;
			transform.localScale = new Vector3 (-1f,1f,1f);
			anim.SetBool("cambio",sentido);
		}

	}

}
