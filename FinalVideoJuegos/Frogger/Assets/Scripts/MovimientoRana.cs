﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoRana : MonoBehaviour {
	static Rigidbody2D cuerpo;
	Animator anim;
	// Use this for initialization
	void Start () {
		cuerpo = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}
	void nos(){
		anim.SetBool("salto",false);
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.W)) {
			cuerpo.transform.Translate (new Vector3 (0, 0.15F, 0));
			EstadoRana.puntua += 10;
			transform.localScale = new Vector3 (1f,1f,1f);
			anim.SetBool("salto",true);
		}
			
		if (Input.GetKeyDown (KeyCode.S)) {
			cuerpo.transform.Translate (new Vector3 (0, -0.15F, 0));
			if (EstadoRana.puntua > 0)
				EstadoRana.puntua -= 10;
			transform.localScale = new Vector3 (1f,-1f,1f);
			anim.SetBool("salto",true);
		}
			
		if (Input.GetKeyDown (KeyCode.D)) {
			cuerpo.transform.Translate (new Vector3 (0.15F, 0, 0));
			mover (0);

		}
		if (Input.GetKeyDown (KeyCode.A)) {
			cuerpo.transform.Translate (new Vector3 (-0.15F, 0, 0));
			mover (0);

		}
		if (Input.GetKey (KeyCode.Q)&&Input.GetKey (KeyCode.E)) {
			EstadoRana.metas = 5;
		}
		if (EstadoRana.vidad == 0) {
			
		}
	}
	public static void mover(float num)
	{
		if(EstadoRana.esmov)
		cuerpo.velocity = new Vector2 (num, 0);
		EstadoRana.esmov = false;
		
	}
	public static void murio()
	{
		cuerpo.transform.position = new Vector3 (0.004999995F, -0.405F, 0);
		EstadoRana.muer = 0;
		Debug.Log ("Vidas=" + EstadoRana.vidad);
	}
	public static void tp()
	{
		cuerpo.transform.position = new Vector3 (0.004999995F, -0.405F, 0);
		EstadoRana.muer = 0;
		EstadoRana.puntua += 50;


	}
}
