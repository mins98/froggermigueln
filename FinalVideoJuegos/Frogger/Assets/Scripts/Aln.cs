﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aln : MonoBehaviour {
	private Rigidbody2D cuerpo;
	// Use this for initialization
	void Start () {
		cuerpo = GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () {
		cuerpo.velocity = new Vector2 (-0.2f, 0);
	}
	void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.name == "Rana") {
			MovimientoRana.mover (-0.2f);
		}
	}
}
