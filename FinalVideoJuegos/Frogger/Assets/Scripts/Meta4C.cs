﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meta4C : MonoBehaviour {

	GameObject ban;
	// Use this for initialization
	void Start () {
		ban = GameObject.Find ("Ban4");
	}

	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D other)
	{

		if (other.gameObject.name == "Rana") {
			if (EstadoRana.met4) {
				EstadoRana.metas += 1;
				EstadoRana.met4 = false;
				Choque.choqu = false;
				MovimientoRana.tp ();
				ban.gameObject.transform.position = new Vector3 (0.391f,1.428f,0);

			}

		}
	}
}
